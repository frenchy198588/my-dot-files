# Aliases for editors
alias vim="nvim"

# Navigation to directories of interest\
alias bb="cd ~/code"
alias confd="cd ~/.config"

# Aliases to edit configs
alias vimconf="vim ~/.config/nvim/init.lua"
alias kittyconf="vim ~/.config/kitty/kitty.conf"
